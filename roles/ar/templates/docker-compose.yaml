networks:
  {{ reverse_proxy_default_network }}:
    external: true

volumes:
  qbittorrent_config:
  jellyfin_cache:
  jellyfin_config:
  radarr_config:
  prowlarr_config:
  sonarr_config:
  jellyseerr_config:

services:

  qbittorrent:
    image: lscr.io/linuxserver/qbittorrent:5.0.3
    container_name: qbittorrent
    hostname: qbittorrent
    restart: unless-stopped
    environment:
      - PUID={{ ar_media_user_id }}
      - PGID={{ ar_media_user_id }}
      - TZ={{ time_zone }}
      - WEBUI_PORT=8080
    volumes:
      - "{{ ar_media_dir_mount }}"
      - qbittorrent_config:/config
    expose:
      - 8080
    networks:
      - {{ reverse_proxy_default_network }}

  jellyfin:
    image: linuxserver/jellyfin:10.10.2
    container_name: jellyfin
    hostname: jellyfin
    restart: unless-stopped
    environment:
      - PUID={{ ar_media_user_id }}
      - PGID={{ ar_media_user_id }}
      - TZ={{ time_zone }}
    ports:
      - 1900:1900/udp
      - 7359:7359/udp
    expose:
      - 8096
      #- 8920 https
    tmpfs:
      - /config/data/transcodes:rw,size=4096M
    volumes:
      - jellyfin_cache:/cache
      - jellyfin_config:/config
      - "{{ ar_media_dir_mount }}"
    devices:
{% for item in ar_media_transcoding_devices %}
      - "{{ item }}:{{ item }}"
{% endfor %}
    networks:
      - {{ reverse_proxy_default_network }}

  radarr:
    image: lscr.io/linuxserver/radarr:latest
    container_name: radarr
    hostname: radarr
    restart: unless-stopped
    environment:
      - PUID={{ ar_media_user_id }}
      - PGID={{ ar_media_user_id }}
      - TZ={{ time_zone }}
    volumes:
      - radarr_config:/config
      - "{{ ar_media_dir_mount }}"
    ports:
      - 7878:7878
    networks:
      - {{ reverse_proxy_default_network }}

  sonarr:
    image: lscr.io/linuxserver/sonarr:latest
    container_name: sonarr
    hostname: sonarr
    restart: unless-stopped
    environment:
      - PUID={{ ar_media_user_id }}
      - PGID={{ ar_media_user_id }}
      - TZ={{ time_zone }}
    volumes:
      - sonarr_config:/config
      - "{{ ar_media_dir_mount }}"
    ports:
      - 8989:8989
    networks:
      - {{ reverse_proxy_default_network }}

  prowlarr:
    image: lscr.io/linuxserver/prowlarr:version-1.30.2.4939
    container_name: prowlarr
    hostname: prowlarr
    restart: unless-stopped
    environment:
      - PUID={{ ar_media_user_id }}
      - PGID={{ ar_media_user_id }}
      - TZ={{ time_zone }}
    volumes:
      - prowlarr_config:/config
    ports:
      - 9696:9696
    networks:
      - {{ reverse_proxy_default_network }}

  jellyseerr:
    image: fallenbagel/jellyseerr:latest
    container_name: jellyseerr
    hostname: jellyseerr
    restart: unless-stopped
    environment:
      - LOG_LEVEL=debug
      - TZ={{ time_zone }}
    ports:
      - 5055:5055
    volumes:
      - jellyseerr_config:/app/config
    networks:
      - {{ reverse_proxy_default_network }}
