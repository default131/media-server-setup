Ansible roles and playbooks to set up a media server, incremental offsite backups. Limited customizability, work in progress.

OVH FQDN refreshing, wildcard certificate through certbot, some services through Docker-compose, Powertop for wattage, Samba shares, backups to remote server(s) with rsnapshot.

```
ansible-playbook -i inventory.yaml playbook_main.yaml
```

To configure the commandline, terminal emulators, install Docker, and configure NFS (needs the inventory file instead) run:
```
ansible-playbook  --connection=local --inventory 127.0.0.1, --tags cmd,docker playbook_cmd.yaml
```
